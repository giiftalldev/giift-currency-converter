<?php
namespace Giift\CurrencyConverter\Traits;

use Http\Discovery\MessageFactoryDiscovery;
use Http\Discovery\UriFactoryDiscovery;
use Http\Message\MessageFactory;
use Http\Message\UriFactory;

trait InjectableFactories
{
    private $mf = null;
    private $uriFactory = null;

    /**
     * @return MessageFactory
     */
    protected function mf(): MessageFactory
    {
        if (is_null($this->mf)) {
            $this->mf = MessageFactoryDiscovery::find();
        }

        return $this->mf;
    }

    /**
     * @return UriFactory
     */
    protected function uriFactory(): UriFactory
    {
        if (is_null($this->uriFactory)) {
            $this->uriFactory = UriFactoryDiscovery::find();
        }

        return $this->uriFactory;
    }
}
