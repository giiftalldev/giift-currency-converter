<?php

namespace Giift\CurrencyConverter\Traits;

use Monolog\Logger;
use Psr\Log\LoggerInterface;

/**
 * Trait HasLogger
 * @package Giift\CurrencyConverter\Traits
 */
trait HasLogger
{
    /** @var  LoggerInterface */
    private $logger;

    /**
     * @return LoggerInterface
     */
    protected function logger(): LoggerInterface
    {
        if (is_null($this->logger)) {
            $logger = new Logger(static::class);
            $this->logger = $logger;
        }
        return $this->logger;
    }

    /**
     * @param LoggerInterface $logger
     * @return static;
     */
    public function setLogger(LoggerInterface $logger)
    {
        $this->logger = $logger;
        return $this;
    }
}
