<?php

namespace Giift\CurrencyConverter\Traits;

use Http\Discovery\HttpClientDiscovery;
use Http\Client\HttpClient;

/**
 * Trait HasHttpClient
 * @package Giift\CurrencyConverter\Traits
 */
trait HasHttpClient
{
    /** @var  HttpClient */
    private $client;

    /**
     * @return HttpClient
     */
    protected function httpClient(): HttpClient
    {
        if (is_null($this->client)) {
            $client = HttpClientDiscovery::find();
            $this->client = $client;
        }
        return $this->client;
    }

    /**
     * @param HttpClient $client
     * @return static
     */
    public function setHttpClient(HttpClient $client)
    {
        $this->client = $client;
        return $this;
    }
}
