<?php

namespace Giift\CurrencyConverter\Traits;

use Psr\SimpleCache\CacheInterface;
use Symfony\Component\Cache\Simple\FilesystemCache;

/**
 * Trait HasCacheHandler
 * @package Giift\CurrencyConverter\Traits
 */
trait HasCacheHandler
{
    /** @var CacheInterface */
    private $cacheHandler;

    /**
     * @return CacheInterface
     * @throws \Exception
     */
    protected function cacheHandler(): CacheInterface
    {
        if (is_null($this->cacheHandler)) {
            $handler = new FilesystemCache('CurrencyConverter');
            $this->cacheHandler = $handler;
        }
        return $this->cacheHandler;
    }

    /**
     * @param CacheInterface $handler
     * @return static
     */
    public function setCacheHandler(CacheInterface $handler)
    {
        $this->cacheHandler = $handler;
        return $this;
    }
}
