<?php

namespace Giift\CurrencyConverter\Providers;

use Giift\CurrencyConverter\Traits\InjectableFactories;
use Psr\Http\Message\ResponseInterface;

/**
 * Class Fixerio
 * @package Giift\CurrencyConverter\Providers
 */
class Fixerio extends Provider implements IProvider
{
    use InjectableFactories;

    /**
     * @param $fromCurrency
     * @param $toCurrency
     * @inheritdoc
     * @throws \Http\Client\Exception
     */
    public function getRate($fromCurrency, $toCurrency)
    {
        $endpoint = "https://api.fixer.io/latest?base=$fromCurrency&symbols=$toCurrency";
        $request = $this->mf()->createRequest('GET', $endpoint);
        try {
            $response = $this->httpClient()->sendRequest($request);
            if ($response->getStatusCode() === 200) {
                return $this->parseRateResponse($response, $fromCurrency, $toCurrency);
            }
            throw new \Exception('Fixerio - getRate - status code was not 200.');
        } catch (\Exception $e) {
            $this->logger()->warning('Fixerio - getRate - Failed to get currency. '.$e->getMessage());
        }
        return null;
    }

    /**
     * @param ResponseInterface $response
     * @param $from
     * @param $to
     * @return float
     * @throws \Exception
     */
    private function parseRateResponse(ResponseInterface $response, $from, $to)
    {
        $body = $response->getBody()->getContents();
        $body = json_decode($body, true);

        if (!isset($body['rates'][$to])) {
            $this->logger()->warning("Fixerio - parseRateResponse - Failed to get currency for $from -> $to.");
            throw new \Exception("Fixerio - parseRateResponse - Failed to get currency for $from -> $to");
        }

        //Pulls the rate from the body.
        $rate = $body['rates'][$to];

        //Some safety checks.
        if (!is_numeric($rate)) {
            $this->logger()->warning("Fixerio - parseRateResponse - Rate is not numeric.");
            throw new \Exception("Fixerio - parseRateResponse - Rate is not numeric.");
        }

        if (!is_float($rate)) {
            $rate = floatval($rate);
        }

        return $rate;
    }
}
