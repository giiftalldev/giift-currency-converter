<?php

namespace Giift\CurrencyConverter\Providers;

use Http\Client\HttpClient;
use Psr\Log\LoggerInterface;
use Giift\CurrencyConverter\Traits\HasLogger;
use Giift\CurrencyConverter\Traits\HasHttpClient;

/**
 * Class Provider
 * @package Giift\CurrencyConverter\Providers
 */
abstract class Provider implements IProvider
{
    use HasLogger;
    use HasHttpClient;

    protected $config;

    /**
     * Provider constructor.
     * @param LoggerInterface|null $logger
     * @param HttpClient|null $client
     * @param array $config
     */
    public function __construct(LoggerInterface $logger = null, HttpClient $client = null, array $config = [])
    {
        //Setting the logger
        if (!is_null($logger)) {
            $this->setLogger($logger);
        }

        //Setting the Http Client
        if (!is_null($client)) {
            $this->setHttpClient($client);
        }

        $this->config = $config;
    }
}
