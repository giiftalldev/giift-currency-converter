<?php

namespace Giift\CurrencyConverter\Providers;

use Http\Client\HttpClient;
use Psr\Log\LoggerInterface;

/**
 * Interface IProvider
 * @package Giift\CurrencyConverter\Providers
 */
interface IProvider
{
    /**
     * IProvider constructor.
     * @param LoggerInterface|null $logger
     * @param HttpClient|null $client
     * @param array $config
     */
    public function __construct(LoggerInterface $logger = null, HttpClient $client = null, array $config = []);

    /**
     * @param string $from
     * @param string $to
     * @return float
     * @throws \Exception
     */
    public function getRate($from, $to);
}
