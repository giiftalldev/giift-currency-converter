<?php

namespace Giift\CurrencyConverter\Providers;

use Giift\CurrencyConverter\Traits\InjectableFactories;
use Http\Discovery\MessageFactoryDiscovery;

/**
 * Class Fixerio
 * @package Giift\CurrencyConverter\Providers
 */
class OpenExchange extends Provider implements IProvider
{
    use InjectableFactories;

    private $baseUrl = 'https://openexchangerates.org/api/';

    /**
     * @return string
     * @throws \Exception
     */
    private function appId(): string
    {
        if (isset($this->config['appId']) && is_string($this->config['appId'])) {
            return $this->config['appId'];
        }
        throw new \Exception('Missing appId from OpenExchange config');
    }

    /**
     * @param string $from
     * @param string $to
     * @return float
     * @throws \Exception
     * @throws \Http\Client\Exception
     */
    public function getRate($from, $to)
    {
        $appId = $this->appId();

        //with the free api, base has to be USD, so we use USD has the pivot $fx1 = 1/(USD->$from) and $fx2 = (usd->$to)
        //and result = $fx1 * $fx2

        //compute $fx1
        if ($from === 'USD') {
            $fx1 = 1.0;
        } else {
            $uri = $this->uriFactory()->createUri("{$this->baseUrl}latest.json")
                ->withQuery("app_id=$appId&symbols=$from");
            $request = MessageFactoryDiscovery::find()->createRequest('GET', $uri);
            $response = $this->httpClient()->sendRequest($request);
            if ($response->getStatusCode() !== 200) {
                throw new \Exception($response->getBody()->getContents());
            }

            $body = json_decode($response->getBody()->getContents(), true);
            $fx1 = 1.0 / $body['rates'][$from];
        }

        //compute $fx2
        if ($to === 'USD') {
            $fx2 = 1.0;
        } else {
            $uri = $this->uriFactory()->createUri("{$this->baseUrl}latest.json")
                ->withQuery("app_id=$appId&symbols=$to");
            $request = MessageFactoryDiscovery::find()->createRequest('GET', $uri);
            $response = $this->httpClient()->sendRequest($request);
            if ($response->getStatusCode() !== 200) {
                throw new \Exception($response->getBody()->getContents());
            }

            $body = json_decode($response->getBody()->getContents(), true);

            $fx2 = $body['rates'][$to];
        }
        return $fx1 * $fx2;
    }
}
