<?php

namespace Giift\CurrencyConverter;

use Giift\Culture\Culture;
use Psr\Log\LoggerInterface;
use Psr\SimpleCache\CacheInterface;
use Giift\CurrencyConverter\Traits\HasLogger;
use Giift\CurrencyConverter\Traits\HasCacheHandler;
use Giift\CurrencyConverter\Providers\IProvider;

/**
 * Class CurrencyConverter
 * @package Giift\CurrencyConverter
 */
class CurrencyConverter
{
    use HasLogger;
    use HasCacheHandler;

    /** @var IProvider[] */
    private $providers = [];

    /** 86400 = 24 hours */
    const TTL = 86400;

    /**
     * CurrencyConverter constructor.
     * @param LoggerInterface|null $logger       Injected logger.
     * @param IProvider[]          $providers    Injected providers.
     * @param CacheInterface|null  $cacheHandler Injected cache.
     */
    public function __construct(
        LoggerInterface $logger = null,
        array $providers = [],
        CacheInterface $cacheHandler = null
    ) {
        //Setting the logger.
        if (!is_null($logger)) {
            $this->setLogger($logger);
        }

        //Setting the providers
        foreach ($providers as $provider) {
            if (!(is_a($provider, IProvider::class))) {
                throw new \InvalidArgumentException('Invalid provider provided!');
            }
            $this->providers[] = $provider;
        }

        //Setting the cacheHandler.
        if (!is_null($cacheHandler)) {
            $this->setCacheHandler($cacheHandler);
        }
    }

    /**
     * @param string $from Currency CCY.
     * @param string $to Currency CCY.
     * @param float|int $amount Amount to convert by rate.
     * @return float
     * @throws \Exception
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function convert(string $from, string $to, float $amount = 1.0)
    {
        //Forcing the currencies to upper case for simplicity.
        $from = trim(strtoupper($from));
        $to = trim(strtoupper($to));

        //Using culture class to check if the ccy is valid.
        $culture = Culture::instance();

        //Safety checks for non-valid ccy.
        if (!$culture->validAlpha3($from)) {
            $this->logger()->error("CurrencyConverter - convert - $from is not valid ccy");
            throw new \InvalidArgumentException("CurrencyConverter - convert - $from is not valid ccy");
        }

        if (!$culture->validAlpha3($to)) {
            $this->logger()->error("CurrencyConverter - convert - $to is not valid ccy");
            throw new \InvalidArgumentException("CurrencyConverter - convert - $to is not valid ccy");
        }

        if ($from === $to) {
            $rate = 1.0;
        } else {
            //Key for caching system.
            $cacheKey = "$from$to";

            //Checking the cache to see if the key exists.
            if ($this->cacheHandler()->has($cacheKey)) {
                $rate = $this->cacheHandler()->get($cacheKey);
            } else {
                $rate = null;
                //Getting the rate from the provider.
                foreach ($this->providers as $provider) {
                    $rate = $provider->getRate($from, $to);
                    if (!is_null($rate)) {
                        $this->cacheHandler()->set($cacheKey, $rate, static::TTL);
                        break;
                    }
                }
                if (is_null($rate)) {
                    $providerClassList = '';
                    foreach ($this->providers as $provider) {
                        $providerClassList .= get_class($provider).' ';
                    }
                    $this->logger()
                        ->error("CurrencyConverter - convert - Failed to get rate. {$from}/{$to} ($providerClassList)");
                    throw new \Exception("CurrencyConverter - convert - Failed to get rate.");
                }
            }
        }

        //Safety check to make sure the amount is numeric.
        if (!is_numeric($amount)) {
            $this->logger()->error("CurrencyConverter - convert - amount is not numeric.");
            throw new \InvalidArgumentException("CurrencyConverter - convert - amount is not numeric.");
        }

        //Finally just returning the rate * amount.
        return $rate * $amount;
    }

    /**
     * @param IProvider $provider
     * @return static
     */
    public function addProvider(IProvider $provider)
    {
        $this->providers[] = $provider;
        return $this;
    }

    /**
     * Clear the current cache.
     * @return static
     * @throws \Exception
     */
    public function resetCache()
    {
        $this->cacheHandler()->clear();
        return $this;
    }
}
