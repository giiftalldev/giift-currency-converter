<?php
namespace Test\Giift\CurrencyConverter;

use Giift\CurrencyConverter\Traits\HasLogger;
use Symfony\Component\Cache\Simple\NullCache;
use Giift\CurrencyConverter\Providers\Fixerio;
use Giift\CurrencyConverter\Providers\MockProvider;

/**
 * Class testCurrencyConverter
 * @package Test\Giift\CurrencyConverter
 * @group CurrencyConverter
 */
class CurrencyConverter extends \PHPUnit_Framework_TestCase
{
    use HasLogger;

    /**
     * Testing with null cache and mock provider;
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function test1()
    {
        $mockProvider = new MockProvider($this->logger());
        $converter = new \Giift\CurrencyConverter\CurrencyConverter($this->logger(), [$mockProvider], new NullCache());
        $rate = $converter->convert('USD', 'EUR');
        static::assertValidRate($rate);
        static::assertEquals($rate, 1.00);
    }

    /**
     * Testing with no config provider and NullCache.
     * @throws \Psr\SimpleCache\InvalidArgumentException
     * @expectedException \Exception
     * @expectedExceptionMessage CurrencyConverter - convert - Failed to get rate.
     */
    public function test2()
    {
        $converter = new \Giift\CurrencyConverter\CurrencyConverter($this->logger(), [], new NullCache());
        $rate = $converter->convert('USD', 'EUR', 50);
        static::assertValidRate($rate);
        static::assertEquals($rate, 50.00);
    }

    /**
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function test3()
    {
        $fixerio = new Fixerio($this->logger());
        $converter = new \Giift\CurrencyConverter\CurrencyConverter($this->logger(), [$fixerio], new NullCache());
        $fixerioRate = $converter->convert('USD', 'EUR');
        static::assertValidRate($fixerioRate);
    }

    /**
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function test4()
    {
        $mockProvider = new MockProvider($this->logger());
        $mockProvider->setRate(0.077);
        $converter = new \Giift\CurrencyConverter\CurrencyConverter($this->logger(), [$mockProvider], new NullCache());
        $rate = $converter->convert('USD', 'EUR');
        static::assertValidRate($rate);
        static::assertEquals($rate, 0.077);
    }

    /**
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function test5()
    {
        $mockProvider = new MockProvider($this->logger());
        $mockProvider->setRate(0.077);
        $converter = new \Giift\CurrencyConverter\CurrencyConverter($this->logger(), [$mockProvider], new NullCache());
        $rate = $converter->convert('USD', 'EUR', 11.00);
        static::assertValidRate($rate);
        static::assertEquals($rate, 0.077*11.00);
    }

    private static function assertValidRate($rate)
    {
        static::assertTrue(is_float($rate));
        static::assertTrue(is_numeric($rate));
    }
}