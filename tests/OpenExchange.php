<?php
namespace Test\Giift\CurrencyConverter;

use Giift\CurrencyConverter\Traits\InjectableFactories;
use Http\Discovery\StreamFactoryDiscovery;
use Http\Mock\Client;
use Ramsey\Uuid\Uuid;

/**
 */
class OpenExchange extends \PHPUnit_Framework_TestCase
{
    use InjectableFactories;

    public function dataProvider()
    {
        return [
            ['USD', 'EUR'],
            ['EUR', 'USD'],
            ['AUD', 'USD'],
            ['USD', 'AUD']
        ];
    }

    /**
     * @param string $from
     * @param string $to
     * @dataProvider dataProvider
     * @throws \Exception
     * @throws \Http\Client\Exception
     */
    public function testGetRate($from, $to)
    {
        $mock = new Client();

        $response = $this->mf()->createResponse(200)
            ->withBody(StreamFactoryDiscovery::find()->createStream(json_encode([
                'rates' => ['EUR' => 2.3, 'AUD' => 2.5]
            ])));
        $mock->addResponse($response);

        $oe = new \Giift\CurrencyConverter\Providers\OpenExchange(
            null,
            $mock,
            ['appId' => Uuid::uuid1()->toString()]
        );
        $v = $oe->getRate($from, $to);
        static::assertInternalType('float', $v);
    }
}
