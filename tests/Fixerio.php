<?php
namespace Test\Giift\CurrencyConverter;

/**
 */
class Fixerio extends \PHPUnit_Framework_TestCase
{
    public function dataProvider()
    {
        return [
            ['USD', 'EUR'],
            ['EUR', 'USD'],
            ['AUD', 'USD'],
            ['USD', 'AUD']
        ];
    }

    /**
     * @param string $from
     * @param string $to
     * @dataProvider dataProvider
     * @throws \Http\Client\Exception
     */
    public function testGetRate($from, $to)
    {
        $fixer = new \Giift\CurrencyConverter\Providers\Fixerio();
        $v = $fixer->getRate($from, $to);
        static::assertInternalType('float', $v);
    }
}
