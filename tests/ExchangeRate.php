<?php
namespace Test\Giift\CurrencyConverter;

/**
 *
 */
class ExchangeRate extends \PHPUnit_Framework_TestCase
{
    public function dataProvider()
    {
        return [
            ['USD', 'KES'],
            ['KES', 'USD'],
            ['QAR', 'USD'],
            ['USD', 'QAR']
        ];
    }

    /**
     * @param string $from
     * @param string $to
     * @dataProvider dataProvider
     * @throws \Http\Client\Exception
     */
    public function testGetRate($from, $to)
    {
        $fixer = new \Giift\CurrencyConverter\Providers\ExchangeRate(null, null, ['key' => 'b2f7180ae4f7d1ebc8213c9b']);
        $v = $fixer->getRate($from, $to);
        static::assertInternalType('float', $v);
    }
}
